from django.contrib import admin
from .models import Order, OrderItem, Request, RequestItem, Incoming, IncomingItem
import csv
from django.http import HttpResponse
from central_supplies.models import Product

# Register your models here.
class IncomingItemInline(admin.TabularInline):
    model = IncomingItem
    raw_id_fields = ['incoming']

@admin.register(Incoming)
class IncomingAdmin(admin.ModelAdmin):
    list_display = ['id',
                    'reference_number',
                    'vendor',
                    'created',
                    'updated']
    list_filter = ['vendor', 'created', 'updated']
    inlines = [IncomingItemInline]

class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']

class RequestItemInline(admin.TabularInline):
    model = RequestItem
    raw_id_fields = ['product']

@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    list_display = ['id',
                    'account',
                    'done_inv',
                    'created',
                    'updated']
    list_filter = ['done_inv', 'created', 'updated']
    inlines = [RequestItemInline]
    actions = ['mark_as_done_invoice']

    def mark_as_done_invoice(self, request, queryset):
        for query in queryset:
            query.done_inv = True
            query.save()

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id',
                    'resident',
                    'forwarded',
                    'created',
                    'updated']
    list_filter = ['forwarded', 'created', 'updated']
    inlines = [OrderItemInline]
    actions = ['mark_as_forwarded', 'export_as_csv']

    def mark_as_forwarded(self, request, queryset):
        for query in queryset:
            print(query)
            query.forwarded = True
            query.save()

    def export_as_csv(self, request, queryset):
        meta = self.model._meta

        # Create a list of items available.
        item_list = ['ITEM NAME']
        for prod in Product.objects.all().order_by('category'):
            item_list.append(prod.name)

        # Create a dictionary of residents each containing a value of
        # a dictionary of items containing a counter with an initial value of 0.
        residents = {}
        for obj in queryset.order_by('resident'):
            d = {}
            for prod in Product.objects.all().order_by('category'):
                d[f'{prod}'] = 0
            residents[f'{obj.resident}'] = d

        # Traverse thru the queryset once more and increment counters according to requested quantities.
        for obj in queryset.order_by('resident'):
            for item in obj.items.all(): #OrderItems foreignkey (Many to one relationship)
                residents[f'{obj.resident}'][f'{item.product}'] += item.quantity

        # Write CSV output file to http response
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)
        #writer.writerow(field_names)

        master_list = []
        master_list.append(item_list)
        for k, v in residents.items():
            l = [k]
            for k, v in v.items():
                l.append(v)
            master_list.append(l)
        
        rcount = 0
        for row in item_list:
            row = writer.writerow((ls[rcount] for ls in master_list))
            rcount += 1

        return response

    export_as_csv.short_description = "Export Selected"

