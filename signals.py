from .models import OrderItem, IncomingItem, Request, RequestItem
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.shortcuts import get_object_or_404

from central_supplies.models import Product, SuppliedResident

@receiver(post_save, sender=OrderItem)
def update_product_and_supplied_resident(sender, instance, created, **kwargs):
    resident = instance.order.resident
    product = instance.product
    if created:
        if product.stock >= instance.quantity:
            product.stock -= instance.quantity
            product.save()
        try:
            supplied_resident = SuppliedResident.objects.get(product=product,
                                                             resident=resident)
            supplied_resident.stock += instance.quantity
            supplied_resident.save()
        except SuppliedResident.DoesNotExist:
            pass

@receiver(post_delete, sender=OrderItem)
def void_order_item(sender, instance, **kwargs):
    product = Product.objects.get(id=instance.product.id)
    product.stock += instance.quantity
    product.save()

def update_product(request_item):
    product = request_item.product
    if product.stock >= request_item.quantity:
        product.stock -= request_item.quantity
        product.save()

@receiver(post_save, sender=Request)
def request_save(sender, instance, created, **kwargs):
    if instance.done_inv:
        for request_item in instance.products.all():
            update_product(request_item)

@receiver(post_delete, sender=RequestItem)
def void_request_item(sender, instance, **kwargs):
    product = instance.product
    product.stock += instance.quantity
    product.save()

@receiver(post_save, sender=IncomingItem)
def update_product_add(sender, instance, created, **kwargs):
    product = instance.product
    product.stock += instance.quantity
    product.save()

@receiver(post_delete, sender=IncomingItem)
def void_incoming_item(sender, instance, **kwargs):
    product = instance.product
    product.stock -= instance.quantity
    product.save()

