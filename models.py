from django.db import models
from central_supplies.models import Product
from geriatrics.models import Admission
from django.conf import settings

# Create your models here.
class Order(models.Model):
    resident = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False}, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    forwarded = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return f'Order {self.id}'

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())

class OrderItem(models.Model):
    order = models.ForeignKey(Order,
                              related_name='items',
                              on_delete=models.CASCADE)
    product = models.ForeignKey(Product,
                                related_name='order_items',
                                on_delete=models.CASCADE, limit_choices_to={'available': True})
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'{self.id}'

    def get_cost(self):
        return self.price * self.quantity

class Request(models.Model):
    account = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    done_inv = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return f'Request {self.id}'

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())

class RequestItem(models.Model):
    request = models.ForeignKey(Request,
                                related_name='products',
                                on_delete=models.CASCADE)
    product = models.ForeignKey(Product,
                                related_name='product_items',
                                on_delete=models.CASCADE, limit_choices_to={'available': True})
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'{self.id}'

    def get_cost(self):
        return self.price * self.quantity

class Incoming(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    reference_number = models.CharField(max_length=200, db_index=True)
    vendor = models.CharField(max_length=200, db_index=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return f'Incoming {self.id}'

class IncomingItem(models.Model):
    incoming = models.ForeignKey(Incoming, related_name='incoming', on_delete=models.CASCADE)
    product = models.ForeignKey(Product,
                                related_name='products',
                                on_delete=models.CASCADE
                                )                                
    quantity = models.PositiveIntegerField(default=1)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f'{self.id}'

    def get_cost(self):
        return self.price * self.quantity

